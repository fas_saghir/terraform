provider "aws" {
  region = "${var.region}"
}

resource "aws_instance" "web" {
  ami           = "ami-00dc79254d0461090"
  instance_type = "t2.micro"

  tags = {
    Name = "${var.env}"
  }
}

resource "aws_security_group" "ingress-all-test" {

name = "faisal-${var.env}-sg"
ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]
from_port = 80
    to_port = 80
    protocol = "tcp"
  }

  egress {
   from_port = 0
   to_port = 0
   protocol = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
}